package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(Main.class);
        logger.info("Application started");

        int[] arr;
        Scanner scanner = new Scanner(System.in);

        if (args.length == 0) {
            arr = new int[10];
            logger.info("Enter 10 numbers");
            for (int i = 0; i < arr.length; i++) {
                var num = scanner.nextInt();
                arr[i] = num;
                logger.info("{} number is {}", i + 1, arr[i]);
            }
        } else if (args.length == 1) {
            arr = new int[]{Integer.parseInt(args[0])};
        } else if (args.length == 10) {
            arr = Arrays.stream(args).mapToInt(Integer::parseInt).toArray();
        } else if (args.length > 10) {
            logger.warn("More than 10 arguments provided, only the first 10 will be used");
            arr = Arrays.stream(args).mapToInt(Integer::parseInt).limit(10).toArray();
        } else {
            throw new IllegalArgumentException("Unexpected number of arguments: " + args.length);
        }

        AscendingSorting.sort(arr);
        logger.info("Sorted array {}", Arrays.toString(arr));
    }
}
