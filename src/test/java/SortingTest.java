import org.example.AscendingSorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingTest {

    private final int[] array;
    private final int[] expected;

    public SortingTest(int[] array, int[] expected) {
        this.array = array;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Iterable<int[][]> testCases() {
        return Arrays.asList(new int[][][]{
                {{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {{-2, -1, -3, -5, -4, 0, 3, 1, 2}, {-5, -4, -3, -2, -1, 0, 1, 2, 3}},
                {{-1000, -500, 100, 500, 5000, 10000, Integer.MAX_VALUE, 1000, 1500, Integer.MIN_VALUE}, {Integer.MIN_VALUE, -1000, -500, 100, 500, 1000, 1500, 5000, 10000, Integer.MAX_VALUE}},
                {{}, {}},
                {{5}, {5}},
                {{11, 3, 8, 15, 2, 10, 1, 14, 6, 9}, {1, 2, 3, 6, 8, 9, 10, 11, 14, 15}},
                {{5, 4, 3, 2, 1, 0, -1, -2, -3, -4, -5}, {-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5}}
        });
    }

    @Test
    public void itSortsAscendingProperly() {
        AscendingSorting.sort(array);
        assertArrayEquals(expected, array);
    }
}